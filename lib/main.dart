import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:em_module4/screens/mainscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nodule 4',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey)
            .copyWith(secondary: Colors.lightBlue),
      ),
      home: AnimatedSplashScreen(
          duration: 3000,
          splash: Text(
            'Welcome to my application',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
          nextScreen: MainScreen(),
          //splashTransition: SplashTransition.fadeTransition,
          //pageTransitionType: PageTransitionType.scale,
          backgroundColor: Colors.blueGrey),
    );
  }
}
